package carfora;

import java.util.Scanner;

/**
 * First part: DFA implementation as simple String analyzing static functions
 * that return true if the String is accepted by the DFA, false otherwise.
 *
 * Exercise number in method signature
 */
public class Part1 {

  /**
   * Esercizio 1.1 versione modificata (automa che riconosce stringhe di 0 e 1
   * che non contengono 3 zeri consecutivi)
   * @param s stringa da riconoscere
   * @return <code>true</code> se la stringa è riconosciuta <code>false</code>
   * altrimenti
   */
  public static boolean scan11(String s) {
    int state = 0;
    int i = 0;
    while (state >= 0 && i < s.length()) {
      final char ch = s.charAt(i++);
      switch (state) {
      case 0:
        if (ch == '0')
          state = 1;
        else if (ch == '1')
          state = 0;
        else
          state = -1;
        break;
      case 1:
        if (ch == '0')
          state = 2;
        else if (ch == '1')
          state = 0;
        else
          state = -1;
        break;
      case 2:
        if (ch == '0')
          state = 3;
        else if (ch == '1')
          state = 0;
        else
          state = -1;
        break;
      case 3:
        if (ch == '0' || ch == '1')
         state = 3;
        else
          state = -1;
        break;
      }
    }
    return state == 0 || state == 1 || state == 2;
  }

  public static boolean scan12(String s) {
    int state = 0;
    int i = 0;
    while (state >= 0 && i < s.length()) {
      final char ch = s.charAt(i++);
      switch(state) {
      case 0:
        if (ch == '+' || ch ==  '-')
          state = 1;
        else if (Character.isDigit(ch))
          state = 2;
        else if (ch == '.')
          state = 3;
        else
          state = -1;
        break;
      case 1:
        if (Character.isDigit(ch))
          state = 2;
        else if (ch == '.')
          state = 3;
        else
          state = -1;
        break;
      case 2:
        if (Character.isDigit(ch))
          state = 2;
        else if (ch == '.')
          state = 3;
        else if (ch == 'e')
          state = 5;
        else
          state = -1;
        break;
      case 3:
        if (Character.isDigit(ch))
          state = 4;
        else
          state = -1;
        break;
      case 4:
        if (Character.isDigit(ch))
          state = 4;
        else if (ch == 'e')
          state = 5;
        else
          state = -1;
        break;
      case 5:
        if (ch == '+' || ch == '-')
          state = 6;
        else if (Character.isDigit(ch))
          state = 7;
        else
          state = -1;
        break;
      case 6:
        if (Character.isDigit(ch))
          state = 7;
        else
          state = -1;
        break;
      case 7:
        if (Character.isDigit(ch))
          state = 7;
        else
          state = -1;
        break;
      }
    }
    return state == 2 || state == 4  || state == 7;
  }

  public static boolean scan13(String s) {
    int state = 0;
    int i = 0;
    while (state >= 0 && i < s.length()) {
      final char ch = s.charAt(i++);
      switch(state) {
      case 0:
        if (ch == '+' || ch ==  '-')
          state = 1;
        else if (Character.isDigit(ch))
          state = 2;
        else if (ch == '.')
          state = 3;
        else if (ch == ' ')
          state = 0;
        else
          state = -1;
        break;
      case 1:
        if (Character.isDigit(ch))
          state = 2;
        else if (ch == '.')
          state = 3;
        else
          state = -1;
        break;
      case 2:
        if (Character.isDigit(ch))
          state = 2;
        else if (ch == '.')
          state = 3;
        else if (ch == 'e')
          state = 5;
        else if (ch == ' ')
          state = 8;
        else
          state = -1;
        break;
      case 3:
        if (Character.isDigit(ch))
          state = 4;
        else
          state = -1;
        break;
      case 4:
        if (Character.isDigit(ch))
          state = 4;
        else if (ch == 'e')
          state = 5;
        else if (ch == ' ')
          state = 8;
        else
          state = -1;
        break;
      case 5:
        if (ch == '+' || ch == '-')
          state = 6;
        else if (Character.isDigit(ch))
          state = 7;
        else
          state = -1;
        break;
      case 6:
        if (Character.isDigit(ch))
          state = 7;
        else
          state = -1;
        break;
      case 7:
        if (Character.isDigit(ch))
          state = 7;
        else if (ch == ' ')
          state = 8;
        else
          state = -1;
        break;
      case 8:
        if (ch == ' ')
          state = 8;
        else
          state = -1;
      }
    }
    return state == 2 || state == 4  || state == 7 || state == 8;
  }

  public static boolean scan14(String s) {
    int state = 0;
    int i = 0;
    while (state >= 0 && i < s.length()) {
      final char ch = s.charAt(i++);
      switch (state) {
      case 0:
        if (ch == '_')
          state = 1;
        else if (Character.isLetter(ch))
          state = 2;
        else
          state = -1;
        break;
      case 1:
        if (ch == '_' || Character.isLetterOrDigit(ch))
          state = 2;
        else
          state = -1;
        break;
      case 2:
        if (ch == '_' || Character.isLetterOrDigit(ch))
          state = 2;
        else
          state = -1;
        break;
      }
    }
    return state == 2;
  }

  public static boolean scan15(String s) {
    int state = 0;
    int i = 0;
    while (state >= 0 && i < s.length()) {
      final char ch = s.charAt(i++);
      switch (state) {
      case 0:
        if (ch == '0')
          state = 0;
        else if (ch == '1')
          state = 1;
        else
          state = -1;
        break;
      case 1:
        if (ch == '1')
          state = 0;
        else if (ch == '0')
          state = 2;
        else
          state = -1;
        break;
      case 2:
        if (ch == '0')
          state = 1;
        else if (ch == '1')
          state = 2;
        else
          state = -1;
        break;
      }
    }
    return state == 0;
  }

  public static void main(String[] args) {
    Scanner kb = new Scanner(System.in);
    System.out.println(scan13(kb.next()) ? "OK" : "NOPE");
  }
}
