package carfora;

import java.util.Scanner;

public class DFATest {

  /**
   * Sets a digit(0-9) move in a DFA
   * @param a the DFA in which the move is to be set
   * @param s the starting state
   * @param f the arriving state
   */
  public static void setDigitMove(DFA a, int s, int f) {
    for (char ch = '0'; ch <= '9'; ch++)
      a.setMove(s, ch, f);
  }

  public static void main(String[] args) {
    // Parte 2
    // DFA figura 1
    DFA fig1 = new DFA(4);
    fig1.setMove(0, '1', 0);
    fig1.setMove(0, '0', 1);
    fig1.setMove(1, '1', 0);
    fig1.setMove(1, '0', 2);
    fig1.setMove(2, '1', 0);
    fig1.setMove(2, '0', 3);
    fig1.setMove(3, '1', 3);
    fig1.setMove(3, '0', 3);
    fig1.addFinalState(3);
    // Es 2.2 (verificare se una stringa passata viene riconosciuta dall'automa
    // in figura 1)
    //Scanner kb = new Scanner(System.in);
    //System.out.println(fig1.scan(kb.next()) ? "OK" : "NOPE");

    // DFA costante numerica floating point
    DFA fl = new DFA(8);
    fl.setMove(0, '+', 1);
    fl.setMove(0, '-', 1);
    setDigitMove(fl, 0, 2);
    fl.setMove(0, '.', 3);
    setDigitMove(fl, 1, 2);
    fl.setMove(1, '.', 3);
    setDigitMove(fl, 2, 2);
    fl.setMove(2, '.', 3);
    fl.setMove(2, 'e', 5);

    setDigitMove(fl, 3, 4);
    setDigitMove(fl, 4, 4);
    fl.setMove(4, 'e', 5);

    fl.setMove(5, '+', 6);
    fl.setMove(5, '-', 6);
    setDigitMove(fl, 5, 7);
    setDigitMove(fl, 6, 7);
    setDigitMove(fl, 7, 7);
    fl.addFinalState(2);
    fl.addFinalState(4);
    fl.addFinalState(7);
    // Es 2.3 (verificare se una stringa passata viene riconosciuta dall'automa
    // riconoscitore di costanti numeriche)
    //Scanner kb = new Scanner(System.in);
    //System.out.println(fl.scan(kb.next()) ? "OK" : "NOPE");

    // DFA parte 3
    DFA p3 = new DFA(5);
    p3.setMove(0, '0', 1);
    p3.setMove(0, '1', 2);
    p3.setMove(1, '0', 1);
    p3.setMove(1, '1', 3);
    p3.setMove(2, '0', 2);
    p3.setMove(2, '1', 2);
    p3.setMove(3, '0', 3);
    p3.setMove(3, '1', 3);
    p3.setMove(4, '0', 3);
    p3.setMove(4, '1', 2);
    p3.addFinalState(3);

    // DFA parte 4
    DFA p4 = new DFA(4);
    p4.setMove(0, '0', 0);
    p4.setMove(0, '1', 1);
    p4.setMove(1, '0', 2);
    p4.setMove(1, '1', 1);
    p4.setMove(2, '0', 2);
    p4.setMove(2, '1', 3);
    p4.setMove(3, '0', 2);
    p4.setMove(3, '1', 3);
    p4.addFinalState(1);
    p4.addFinalState(3);

    DFA min = new DFA(2);
    min.setMove(0, '0', 0);
    min.setMove(0, '1', 1);
    min.addFinalState(1);
    //System.out.println(p4.minimize().equivalentTo(p4));
    //System.out.println(fig1.complete());
    //fl.toDOT("Floating");
    /*
    for (String s : fl.samples()) {
      System.out.println(s);
    }
    p4.minimize().toDOT("p4");
    */
  }
}
