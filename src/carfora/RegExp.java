package carfora;

public interface RegExp {
  NFA compile();
}
