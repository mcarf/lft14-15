package carfora;

/**
 *
 * @author Matteo Carfora
 */
public class RegExpChoice implements RegExp {
  private RegExp e1;
  private RegExp e2;

  RegExpChoice(RegExp e1, RegExp e2) {
    this.e1 = e1;
    this.e2 = e2;
  }

  @Override
  public NFA compile() {
    NFA a = new NFA(2);
    final int n = a.append(e1.compile());
    final int m = a.append(e2.compile());
    a.addMove(0, NFA.EPSILON, n);
    a.addMove(0, NFA.EPSILON, m);
    a.addMove(n + 1, NFA.EPSILON, 1);
    a.addMove(m + 1, NFA.EPSILON, 1);
    a.addFinalState(1);
    return a;
  }

  public static RegExp range(char from, char to) {
    RegExp r = new RegExpSymbol(from);
    for (char c = (char)(from + 1); c <= to; c++)
      r = new RegExpChoice(r, new RegExpSymbol(c));
    return r;
  }

}
