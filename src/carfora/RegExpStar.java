package carfora;

/**
 *
 * @author Matteo Carfora
 */
public class RegExpStar implements RegExp {
  private RegExp e;

  public RegExpStar(RegExp e) {
    this.e = e;
  }

  @Override
  public NFA compile() {
    NFA a = new NFA(2);
    final int n = a.append(e.compile());
    a.addMove(0, NFA.EPSILON, 1);
    a.addMove(0, NFA.EPSILON, n);
    a.addMove(n + 1, NFA.EPSILON, 1);
    a.addMove(1, NFA.EPSILON, n);
    a.addFinalState(1);
    return a;
  }

}
