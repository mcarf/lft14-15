package carfora;

/**
 *
 * @author Matteo Carfora
 */
public class NFATest {
  public static void main(String[] args) {
    NFA p5 = new NFA(4);
    p5.addMove(0, '0', 1);
    p5.addMove(0, '1', 1);
    p5.addMove(0, '1', 2);
    p5.addMove(1, '1', 3);
    p5.addMove(2, '0', 3);
    p5.addFinalState(3);

    NFA test = new NFA(4);
    test.addMove(0, 'b', 1);
    test.addMove(0, 'c', 2);
    test.addMove(0, NFA.EPSILON, 3);
    test.addMove(1, 'b', 1);
    test.addMove(1, 'b', 2);
    test.addMove(1, NFA.EPSILON, 2);
    test.addMove(1, 'c', 3);
    test.addMove(2, 'c', 1);
    test.addMove(2, 'c', 2);
    test.addMove(2, 'b', 3);
    test.addMove(3, 'a', 3);
    test.addFinalState(3);

    /* (/|c|*˟c)˟*˟ */
    RegExp slash = new RegExpSymbol('/');
    RegExp c = new RegExpSymbol('c');
    RegExp s = new RegExpSymbol('*');
    RegExp sstar = new RegExpStar(s);
    RegExp slashs = new RegExpSequence(slash, s);
    RegExp sslash = new RegExpSequence(s, slash);
    RegExp sstarc = new RegExpSequence(sstar, c);
    RegExp center = new RegExpStar(new RegExpChoice(new RegExpChoice(slash, c), sstarc));
    RegExp es62 = new RegExpSequence(slashs, new RegExpSequence(center, new RegExpSequence(sstar, sslash)));
    //es62.compile().dfa().minimize().toDOT("es62");

    /*
    new RegExpStar(new RegExpChoice(new RegExpSymbol('a'), new RegExpSymbol('b')))
            .compile().dfa().minimize().toDOT("es63");
    */

    /* (+|-|ε)(n+|.n+|n+.n+)(e(+|-|ε)n+|ε) */
    RegExp plus = new RegExpSymbol('+');
    RegExp minus = new RegExpSymbol('-');
    RegExp dot = new RegExpSymbol('.');
    RegExp e = new RegExpSymbol('e');
    RegExp eps = new RegExpEpsilon();
    RegExp n = RegExpChoice.range('0', '9');
    RegExp np = new RegExpSequence(n, new RegExpStar(n));
    RegExp op = new RegExpChoice(plus, new RegExpChoice(minus, eps));
    RegExp decimal = new RegExpSequence(dot, np);
    RegExp exp = new RegExpSequence(e, new RegExpSequence(op, np));
    RegExp fl = new RegExpSequence(op, new RegExpChoice(np,
            new RegExpChoice(decimal, new RegExpSequence(np, decimal))));
    RegExp flexp = new RegExpSequence(fl, new RegExpChoice(exp, eps));
    flexp.compile().dfa().minimize().toDOT("Prova");
  }
}
