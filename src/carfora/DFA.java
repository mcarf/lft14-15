package carfora;

import java.util.HashSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

/**
 * Un oggetto della classe DFA rappresenta un automa a stati finiti
 * deterministico
 */
public class DFA
{
    /**
     * Numero degli stati dell'automa. Ogni stato e` rappresentato da
     * un numero interno non negativo, lo stato con indice 0 e` lo
     * stato iniziale.
     */
    private int numberOfStates;

    /** Insieme degli stati finali dell'automa. */
    private HashSet<Integer> finalStates;

    /**
     * Funzione di transizione dell'automa, rappresentata come una
     * mappa da mosse a stati di arrivo.
     */
    private HashMap<Move, Integer> transitions;

    /**
     * Crea un DFA con un dato numero di stati.
     * @param  n Il numero di stati dell'automa.
     */
    public DFA(int n) {
	numberOfStates = n;
	finalStates = new HashSet<Integer>();
	transitions = new HashMap<Move, Integer>();
    }

    /**
     * Aggiunge uno stato all'automa.
     * @return L'indice del nuovo stato creato
     */
    public int newState() {
	return numberOfStates++;
    }

    /**
     * Aggiunge una transizione all'automa.
     * @param  p  Lo stato di partenza della transizione.
     * @param  ch Il simbolo che etichetta la transizione.
     * @param  q  Lo stato di arrivo della transizione.
     * @return <code>true</code> se lo stato di partenza e lo stato di
     *         arrivo sono validi, <code>false</code> altrimenti.
     */
    public boolean setMove(int p, char ch, int q) {
	if (!validState(p) || !validState(q))
	    return false;

	transitions.put(new Move(p, ch), q);
	return true;
    }

  /**
   * Aggiunge una transizione con una cifra(0-9) all'automa
   * @param p Lo stato di partenza
   * @param q Lo stato di arrivo
   */
  public void setDigitMove(int p, int q) {
    for (char ch = '0'; ch <= '9'; ch++)
      setMove(p, ch, q);
  }

    /**
     * Aggiunge uno stato finale.
     * @param  p Lo stato che si vuole aggiungere a quelli finali.
     * @return <code>true</code> se lo stato e` valido,
     *         <code>false</code> altrimenti.
     */
    public boolean addFinalState(int p) {
	if (validState(p)) {
	    finalStates.add(p);
	    return true;
	} else
	    return false;
    }

    /**
     * Determina se uno stato e` valido oppure no.
     * @param  p Lo stato da controllare.
     * @return <code>true</code> se lo stato e` valido,
     *         <code>false</code> altrimenti.
     * @see #numberOfStates
     */
    public boolean validState(int p) {
	return (p >= 0 && p < numberOfStates);
    }

    /**
     * Determina se uno stato e` finale oppure no.
     * @param  p Lo stato da controllare.
     * @return <code>true</code> se lo stato e` finale,
     *         <code>false</code> altrimenti.
     * @see #finalStates
     */
    public boolean finalState(int p) {
	return finalStates.contains(p);
    }

    /**
     * Restituisce il numero di stati dell'automa.
     * @return Numero di stati.
     */
    public int numberOfStates() {
	return numberOfStates;
    }

    /**
     * Restituisce l'alfabeto dell'automa, ovvero l'insieme di simboli
     * che compaiono come etichette delle transizioni dell'automa.
     * @return L'alfabeto dell'automa.
     */
    public HashSet<Character> alphabet() {
	HashSet<Character> alphabet = new HashSet<Character>();
	for (Move m : transitions.keySet())
	    alphabet.add(m.ch);
	return alphabet;
    }

    /**
     * Esegue una mossa dell'automa.
     * @param p  Stato di partenza prima della transizione.
     * @param ch Simbolo da riconoscere.
     * @return Stato di arrivo dopo la transizione, oppure
     *         <code>-1</code> se l'automa non ha una transizione
     *         etichettata con <code>ch</code> dallo stato
     *         <code>p</code>.
     */
    public int move(int p, char ch) {
	Move move = new Move(p, ch);
	if (transitions.containsKey(move))
	    return transitions.get(move);
	else
	    return -1;
    }

    /**
     * Verifica se una stringa e` riconosciuta dall'automa.
     * @param  s  Stringa da riconoscere.
     * @return <code>true</code> se la stringa e` stata riconosciuta,
     *         <code>false</code> altrimenti.
     */
    public boolean scan(String s) {
      int state = 0;
      int i = 0;
      while (state >= 0 && i < s.length()) {
        final char ch = s.charAt(i++);
        Move m = new Move(state, ch);
        Integer stValue = transitions.get(m);
        state = stValue != null ? stValue : -1;
      }
      return finalStates.contains(state);
    }

    /**
     * Verfica se la funzione di transizione dell'automa è definita
     * per tutti gli stati dell'automa e i simboli dell'alfabeto.
     * @return <code>true</code> se la condizione è verificata,
     *         <code>false</code> altrimenti.
     */
    public boolean complete() {
      HashSet<Character> alphabet = alphabet();
      for (int i = 0; i < numberOfStates; i++) {
        for (char c : alphabet) {
          if (transitions.get(new Move(i,c)) == null)
            return false;
        }
      }
      return true;
    }

    /**
     * Stampa una rappresentazione testuale dell'automa da
     * visualizzare con <a href="http://www.graphviz.org">GraphViz</a>.
     * @param name Nome dell'automa.
     */
    public void toDOT(String name) {
      StringBuilder res = new StringBuilder();
      res.append("digraph ").append(name).append(" {\n");
      res.append("  rankdir=LR;\n");
      res.append("  node [shape = doublecircle];\n");

      for (int state : finalStates)
        res.append("  q").append(state).append(";\n");
      res.append("  node [shape = circle];\n");

      // ciclo di stampa archi
      for (Entry<Integer, HashMap<Integer, LinkedList<Character>>> outgoing
              : edges().entrySet()) {
        for (Entry<Integer, LinkedList<Character>> edge
                : outgoing.getValue().entrySet()) {
          res.append("  q");
          res.append(outgoing.getKey());
          res.append(" -> q");
          res.append(edge.getKey());
          res.append(" [ label = \"");
          String delim = "";
          for (char c : edge.getValue()) {
            res.append(delim).append(c);
            delim = ",";
          }
          res.append("\" ];\n");
        }
      }
      res.append("}\n");
      System.out.println(res);
    }

    /**
     * Stampa una classe Java con un metodo <code>scan</code> che implementa
     * l'automa.
     * @param name Nome della classe da generare.
     */
    public void toJava(String name) {
      StringBuilder res = new StringBuilder();
      res.append("public class ").append(name).append(" {\n");
      res.append("  public static boolean scan(String s) {\n");
      res.append("    int state = 0;\n");
      res.append("    int i = 0;\n");
      res.append("    while (state >= 0 && i < s.length()) {\n");
      res.append("      final char ch = s.charAt(i++);\n");
      res.append("\n");
      res.append("      switch (state) {\n");
      // ciclo "case"
      for (Entry<Integer, HashMap<Integer, LinkedList<Character>>> outgoing
              : edges().entrySet()) {
        res.append("      case ").append(outgoing.getKey()).append(":\n");
        String delim = "        if (";
        // ciclo assegnazione stato
        for (Entry<Integer, LinkedList<Character>> edge :
                outgoing.getValue().entrySet()) {
          res.append(delim);
          delim = "";
          // ciclo condizioni
          for (char ch : edge.getValue()) {
            res.append(delim).append("ch == '").append(ch).append('\'');
            delim = " || ";
          }
          res.append(")\n");
          res.append("          state = ").append(edge.getKey()).append(";\n");
          delim = "        else if (";
        }
        res.append("        else\n");
        res.append("          state = -1;\n");
        res.append("        break;\n");
      }
      res.append("      }\n");
      res.append("    }\n");
      res.append("    return ");
      String delim = "";
      for (int state : finalStates) {
        res.append(delim).append("state == ").append(state);
        delim = " || ";
      }
      res.append(";\n");
      res.append("  }\n\n");
      res.append("  public static void main(String[] args) {\n");
      res.append("    System.out.println(scan(args[0]) ? \"OK\" : \"NOPE\");\n");
      res.append("  }\n");
      res.append("}\n");
      System.out.println(res);
    }

    /**
     * Dato uno stato dell'automa ritorna l'insieme degli stati raggiungibili
     * da esso
     * @param q uno stato dell'automa
     * @return l'insieme degli stati raggiungibili se <code>state</code>
     * appartiene agli stati del DFA <code>null</code> altrimenti
     */
    public HashSet<Integer> reach(int q) {
      if (!validState(q)) return null;
      boolean[] r = new boolean[numberOfStates];
      r[q] = true;
      HashMap<Integer, HashMap<Integer, LinkedList<Character>>> edges = edges();
      boolean newPair = true;
      while(newPair) {
        newPair = false;
        for (int i = 0; i < numberOfStates; i++) {
          for (int j = 0; j < numberOfStates; j++) {
            LinkedList<Character> transition = edges.get(i).get(j);
            if (r[i] && transition != null && !transition.isEmpty()) {
              if (!r[j]) {
                r[j] = true;
                newPair = true;
              }
            }
          }
        }
      }
      /*
      LinkedList<Integer> border = new LinkedList<>();
      border.add(q);
      while (!border.isEmpty()) {
        int i = border.remove();
        for (int j : edges.get(i).keySet()) {
          if (!r[j]) {
            border.add(j);
            r[j] = true;
          }
        }
      }
      */
      HashSet<Integer> s = new HashSet<>();
      for (int i = 0; i < r.length; i++)
        if (r[i]) s.add(i);
      return s;
    }

    /**
     * Verifica se l'automa riconosce il linguaggio vuoto
     * @return <code>true</code> se l'automa riconosce il linguaggio vuoto
     * <code>false</code> altrimenti
     */
    public boolean empty() {
      for (int i : reach(0))
        if (finalState(i)) return false;
      return true;
    }

    /**
     * Restituisce l'insieme degli stati pozzo dell'automa
     * @return l'insieme degli stati pozzo
     */
    public HashSet<Integer> sink() {
      HashSet<Integer> sink = new HashSet<>();
      for (int i = 0; i < numberOfStates; i++) {
        boolean s = true;
        for (int j : reach(i)) {
          if (finalState(j)) {
            s = false;
            break;
          }
        }
        if (s) sink.add(i);
      }
      return sink;
    }

    /**
     * Restituisce un insieme di stringhe accettate dall'automa,
     * una per ogni stato finale dell'automa
     * @return l'insieme delle stringhe
     */
    public HashSet<String> samples() {
      String[] r = new String[numberOfStates];
      HashMap<Integer, HashMap<Integer, LinkedList<Character>>> edges = edges();
      r[0] = "";
      boolean newPair = true;
      while(newPair) {
        newPair = false;
        for (int i = 0; i < numberOfStates; i++) {
          for (int j = 0; j < numberOfStates; j++) {
            LinkedList<Character> transition = edges.get(i).get(j);
            if (r[i] != null && transition != null && !transition.isEmpty()) {
              if (r[j] == null) {
                r[j] = r[i] + transition.getFirst();
                newPair = true;
              }
            }
          }
        }
      }
      /*
      LinkedList<Integer> border = new LinkedList<>();
      border.add(0);
      while (!border.isEmpty()) {
        int i = border.remove();
        for (Entry<Integer, LinkedList<Character>> edge
                : edges.get(i).entrySet()) {
          int j = edge.getKey();
          char ch = edge.getValue().peek();
          if (r[j] == null) {
            border.add(j);
            r[j] = r[i] + ch;
          }
        }
      }
      */
      HashSet<String> samples = new HashSet<>();
      for (int i : finalStates)
        samples.add(r[i]);
      return samples;
    }

    /**
     * Restituisce una mappa da uno stato ai suoi archi uscenti, gli archi
     * con stesso stato iniziale e di arrivo vengono raggruppati in un insieme
     * @return la mappa degli archi
     */
    private HashMap<Integer, HashMap<Integer, LinkedList<Character>>> edges() {
      HashMap<Integer, HashMap<Integer, LinkedList<Character>>> edges =
              new HashMap<>();
      for (Entry<Move, Integer> entry : transitions.entrySet()) {
        HashMap<Integer, LinkedList<Character>> outgoing =
                edges.get(entry.getKey().start);
        if (outgoing == null) {
          outgoing = new HashMap<>();
          edges.put(entry.getKey().start, outgoing);
        }
        LinkedList<Character> edge = outgoing.get(entry.getValue());
        if (edge == null) {
          edge = new LinkedList<>();
          outgoing.put(entry.getValue(), edge);
        }
        edge.add(entry.getKey().ch);
      }
      return edges;
    }

    /**
     * Restituisce un nuovo DFA che accetta lo stesso linguaggio riconosciuto
     * da questo DFA con il minimo numero di stati possibile.
     * @return il DFA minimo equivalente
     */
    public DFA minimize() {
      boolean[][] eq = findEq(null);
      int[] m = new int[numberOfStates];
      int k = 0;
      for (int i = 0; i < numberOfStates; i++) {
        for (int j = 0; j < numberOfStates; j++) {
          if (eq[i][j]) {
            m[i] = j;
            if (j > k)
              k = j;
            break;
          }
        }
      }
      DFA min = new DFA(k + 1);
      for (Entry <Move, Integer> transition : transitions.entrySet()) {
        Move mv = transition.getKey();
        min.setMove(m[mv.start], mv.ch, m[transition.getValue()]);
      }
      for (int i = 0; i < m.length; i++)
        if (finalState(i))
          min.addFinalState(m[i]);
      return min;
    }

    /**
     * Verifica se un'altro dfa è equivalente a questo,
     * cioè se riconoscono lo stesso linguaggio
     * @param a l'automa da verificare
     * @return <code>true</code> se gli automi sono equivalenti
     * <code>false</code> altrimenti
     */
    public boolean equivalentTo(DFA a) {
      return findEq(a)[0][numberOfStates];
    }

    /**
     * Ricerca degli stati indistinguibili
     *
     * <p>la cella (i,j) avrà valore <code>true</code> se la
     * coppia di stati (i,j) è indistinguibile <code>false</code> altrimenti<p/>
     *
     * @param a dfa opzionale per controllo equivalenza
     * @return matrice degli stati indistinguibili
     */
    @SuppressWarnings("null")
    private boolean[][] findEq(DFA a) {
      // dfa unione se il dfa opzionale viene passato
      final int n = numberOfStates;
      DFA un = new DFA(n + (a != null ? a.numberOfStates : 0));
      un.transitions.putAll(transitions);
      un.finalStates.addAll(finalStates);
      if (a != null) {
        for (Entry<Move, Integer> e : a.transitions.entrySet())
          un.setMove(n + e.getKey().start, e.getKey().ch, n + e.getValue());
        for (int f : a.finalStates)
          un.addFinalState(n + f);
      }
      boolean[][] eq = new boolean[un.numberOfStates][un.numberOfStates];
      for (int i = 0; i < eq.length; i++)
        for (int j = 0; j < eq[0].length; j++)
          eq[i][j] = un.finalState(i) == un.finalState(j);

      HashSet<Character> alphabet = un.alphabet();
      boolean newPair = true;
      while (newPair) {
        newPair = false;
        for (int i = 0; i < eq.length; i++) {
          for (int j = 0; j < eq[0].length; j++) {
            for (char ch : alphabet) {
              int iarr = un.move(i, ch);
              int jarr = un.move(j, ch);
              // entrambi errore o validi
              boolean same  = (iarr == -1) == (jarr == -1);
              // entrambi validi
              boolean valid = (iarr != -1) && (jarr != -1);
              // i e j distinguibili se solo uno arriva nello stato di errore
              // o se entrambi arrivano in stati distinguibili
              if (eq[i][j] && (!same || (valid && !eq[iarr][jarr]))) {
                eq[i][j] = false;
                newPair = true;
              }
            }
          }
        }
      }
      return eq;
    }
}