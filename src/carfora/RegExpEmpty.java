package carfora;

/**
 *
 * @author Matteo Carfora
 */
public class RegExpEmpty implements RegExp{

  @Override
  public NFA compile() {
    NFA a = new NFA(2);
    a.addFinalState(1);
    return a;
  }

}
